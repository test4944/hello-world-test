# Maven web application project

### Clean fresh application code is available in clean branch and clean tag
To clone the clean branch
```
git clone -b clean https://github.com/vigneshsweekaran/hello-world.git
```
### To generate the package
```
mvn clean package
```
### War file is generated in target/hello-world.war

### Kaniko
##### Push to Dockerhub
* Two variables needs to be created DOCKER_USERNAME and DOCKER_PASSWORD

##### Push to AWS ECR
* Two variables needs to be created AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY
